'use strict';

global.$ = {
  package: require('./package.json'),
  config: require('./gulp/config'),
  path: {
    task: require('./gulp/paths/tasks.js'),
    jsFoundation: require('./gulp/paths/js.foundation.js'),
    cssFoundation: require('./gulp/paths/css.foundation.js'),
    app: require('./gulp/paths/app.js')
  },
  gulp: require('gulp'),
  rimraf: require('rimraf'),
  browserSync: require('browser-sync').create(),
  gp: require('gulp-load-plugins')()
};

$.path.task.forEach(function (taskPath) {
  require(taskPath)();
});

$.gulp.task('default', $.gulp.series(
  'clean',
  'sprite:img',
  $.gulp.parallel(
    'sass',
    'pug',
    'js:foundation',
    'js:process',
    'copy:image',
    'copy:fonts',
    'copy:php',
    'copy:other',
    'css:foundation'
  ),
  $.gulp.parallel(
    'watch',
    'serve:php',
    'serve'
  )
));

$.gulp.task('build', $.gulp.series(
  'clean',
  'sprite:img',
  $.gulp.parallel(
    'optimize:sass',
    'optimize:js',
    'optimize:image',
    'css:foundation',
    'pug',
    'js:foundation',
    'copy:fonts',
    'copy:php',
    'copy:other'
  )
));