<?php


class App extends Controller
{
  function getData()
  {
    $f3 = Base::instance();
    $db = $this->db;

    $blocks = new DB\SQL\Mapper($db, 'blocks');
    $section = new DB\SQL\Mapper($db, 'sections');
    $common = new DB\SQL\Mapper($db, 'common');
    $dish = new DB\SQL\Mapper($db, 'dishes');
    $blocks->load();
    $section->load();
    $common->load();

    // Создаем вложенные массивы с данными о секциях и блюдах
    $result = array();

    do {
      $curSection = $section->cast();

      // Если блюда в данной сеции есть
      $dish->load(array('section_id=?', $section->id));

      $curSection['dishes'] = array();

      if ($dish->count()) {
        do {
          array_push($curSection['dishes'], $dish->cast());
        } while ($dish->next());
      }

      array_push($result, $curSection);

    } while ($section->next());

    // Массив блоков
    $blocksData = array();

    do {
    	$block = $blocks->cast();

       $blocksData[$block['block_name']] = $block;
    } while ($blocks->next());

    // Выкидываем в переменную
    $f3->set('blocks', $blocksData);
    $f3->set('sections', $result);
    $f3->set('types', array('light', 'medium', 'hard'));
    $f3->set('common', $common->cast());
  }

  function home($f3)
  {
    $this->getData();

    echo Template::instance()->render('index.html');
  }

  function login($f3)
  {
    if ($f3->get('SESSION.username') == $f3->get('username') && $f3->get('SESSION.password') == $f3->get('password')) {
      $f3->reroute('/admin');
    }

    echo Template::instance()->render('login.html');
  }

  function auth($f3)
  {
    if ($f3->get('POST.username') == $f3->get('username') && md5($f3->get('POST.password')) == $f3->get('password')) {
      $f3->set('SESSION.username', $f3->get('POST.username'));
      $f3->set('SESSION.password', $f3->get('password'));
    }
    $f3->reroute('/login');
  }

  function admin($f3)
  {
    if ($f3->get('SESSION.username') != $f3->get('username') || $f3->get('SESSION.password') != $f3->get('password')) {
      $f3->clear('SESSION');
      $f3->reroute('/login');
    }

    $this->getData();
//    echo '<pre>';
//    var_dump($f3->get('sections'));
//    echo  '</pre>';

    echo Template::instance()->render('admin.html');
  }

  function sectionAdd($f3)
  {
    $db = $this->db;

    $section = new DB\SQL\Mapper($db, 'sections');

    $section->copyfrom('POST');

    $section->insert();

    $f3->reroute('/admin');
  }

  function sectionDelete($f3)
  {
    $db = $this->db;

    $section = new DB\SQL\Mapper($db, 'sections');
    $dish = new DB\SQL\Mapper($db, 'dishes');

    $section->load(array('id=?', $f3->get('POST.id')));
    $dish->load(array('section_id=?', $f3->get('POST.id')));

    // Удаление блюд секции
    if ($dish->count()) {
      do {
        $dish->erase();
      } while ($dish->next());
    }

    $section->erase();

    $f3->reroute('/admin');
  }

  function sectionEdit($f3)
  {
    $db = $this->db;

    $section = new DB\SQL\Mapper($db, 'sections');

    $section->load(array('id=?', $f3->get('POST.id')));

    $section->copyfrom('POST');

    $section->update();

    $f3->reroute('/admin');
  }

  function dishAdd($f3)
  {
    $db = $this->db;

    $dish = new DB\SQL\Mapper($db, 'dishes');

    $dish->copyfrom('POST');

    $dish->insert();

    $f3->reroute('/admin');
  }

  function dishEdit($f3)
  {
    $db = $this->db;

    $dish = new DB\SQL\Mapper($db, 'dishes');

    $dish->load(array('id=?', $f3->get('POST.id')));

    $dish->copyfrom('POST');

    $dish->update();

    $f3->reroute('/admin');
  }

  function dishDelete($f3)
  {
    $db = $this->db;

    $dish = new DB\SQL\Mapper($db, 'dishes');

    $dish->load(array('id=?', $f3->get('POST.id')));

    $dish->erase();

    $f3->reroute('/admin');
  }

  function commonEdit($f3)
  {
    $db = $this->db;

    $common = new DB\SQL\Mapper($db, 'common');

    $common->load(array('id=?', $f3->get('POST.id')));

    $common->copyfrom('POST');

    $common->update();

    $f3->reroute('/admin');
  }

  function blockEdit($f3)
  {
     $db = $this->db;

     $blocks = new DB\SQL\Mapper($db, 'blocks');

	  $blocks->load(array('id=?', $f3->get('POST.id')));

	  $blocks->copyfrom('POST');

     $isEnabled = $f3->get('POST.enable');

     if (empty($isEnabled)) {
	     $blocks->enable = 0;
     }

	  $blocks->update();

     $f3->reroute('/admin');
  }

  function sendMail($f3)
  {
    if (!$f3->exists('POST.phone')) die("Вы не ввели свой номер телефона!");

    $mail = new PHPMailer;

    // Настройка SMTP
    $smtp_config = $f3->get('smtp');

    if ($smtp_config['use'] == 'true') {
      $mail->isSMTP();
      $mail->SMTPDebug = 0;
      $mail->Host = $smtp_config['host'];
      $mail->SMTPAuth = true;
      $mail->Username = $smtp_config['user'];
      $mail->Password = $smtp_config['pass'];
      $mail->SMTPSecure = $smtp_config['secure'];
      $mail->Port = $smtp_config['port'];
    }

    // Настройки письма
    $mail_config = $f3->get('mail');

    $mail->CharSet = 'utf-8';
    $mail->setFrom($mail_config['sender_addr'], $mail_config['sender_name'], 0);

    // Получатели письма
    if ($f3->exists('POST.mailto')) {
      // Если у формы указан получатель(ли)

      foreach ($f3->get('POST.mailto') as $key => $val) {
        $mail->AddAddress(trim($val));
      }
    } else {
    	if (is_array($mail_config['recipients'])) {
		    foreach ($mail_config['recipients'] as $recipient) {
			    $mail->addAddress($recipient); // Добавление получателя
		    }
	    } else {
		    $mail->addAddress($mail_config['recipients']);
	    }
    }

    $mail->isHTML(true);
    $mail->Subject = $mail_config['subject'];
    $mail->Body = $mail_config['body'];
    $mail->AltBody = $mail_config['altbody'];

    // Формируем письмо
    $data = $f3->get('POST');
    foreach ($mail_config['fields'] as $k => $v) {
      if (isset($data[$k])) {
        $mail->Body .= "<b>$v:</b> " . $data[$k] . "<br>";
        $mail->AltBody .= "$v: " . $data[$k] . "\n";
      }
    }

    date_default_timezone_set('Europe/Moscow');
    $mail->Body .= "<b>Дата:</b> " . date("m.d.y H:i:s") . "<br>";
    $mail->AltBody .= "Дата: " . date("m.d.y H:i:s") . "\n";

    if (!$mail->send()) {
//      echo 'Ошибка: ' . $mail->ErrorInfo;
      header('HTTP/1.1 500 Internal Server Error');
      die();
    } else {
//      echo 'Заявка усппешно отправлена.';
      exit();
    }
  }

  function logout($f3)
  {
    $f3->clear('SESSION');
  }
}