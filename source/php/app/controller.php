<?php

//! Base controller
class Controller
{
  protected
      $db;

  function beforeroute($f3)
  {
    $db = $this->db;
  }

  //! Instantiate class
  function __construct()
  {
    $f3 = Base::instance();

    // Connect to the database
    $db_config = $f3->get('db');
    $db = new DB\SQL('sqlite:' . $db_config['path']);

    if (file_exists('setup.sql')) {
      // Initialize database with default setup
      $db->exec(explode(';', $f3->read('setup.sql')));
      // Make default setup inaccessible
      rename('setup.sql', 'setup.$ql');
    }
    // Use database-managed sessions
//    new DB\SQL\Session($db);
    // Save frequently used variables
    $this->db = $db;
  }
}
