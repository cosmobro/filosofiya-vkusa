CREATE TABLE IF NOT EXISTS `common` (
  `id`         INTEGER PRIMARY KEY   AUTOINCREMENT,
  `cost_total` VARCHAR(255)          DEFAULT NULL,
  `date`       VARCHAR(255) NOT NULL,
  `cost1`      VARCHAR(255) NOT NULL,
  `cost2`      VARCHAR(255) NOT NULL,
  `cost3`      VARCHAR(255) NOT NULL,
  `cost4`      VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS `dishes` (
  `id`         INTEGER PRIMARY KEY   AUTOINCREMENT,
  `name`       TEXT,
  `text`       TEXT,
  `section_id` INT(11)      NOT NULL,
  `type`       VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS `sections` (
  `id`   INTEGER PRIMARY KEY   AUTOINCREMENT,
  `name` TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS `blocks` (
  `id`   INTEGER PRIMARY KEY   AUTOINCREMENT,
  `header` TEXT,
  `price` TEXT,
  `block_name` TEXT,
  `enable` INT(1)
);

INSERT INTO common (cost_total, date, cost1, cost2, cost3, cost4) VALUES ('250', '00.00.00', '40', '40', '50', '50');

INSERT INTO sections (name) VALUES ('САЛАТЫ (130г):');
INSERT INTO sections (name) VALUES ('Первое блюдо на выбор (200 г.):');
INSERT INTO sections (name) VALUES ('Второе блюдо на выбор (150 г.):');
INSERT INTO sections (name) VALUES ('Гарнир на выбор (130 г.):');

INSERT INTO blocks (header, price, block_name, enable) VALUES ('Экспресс похудение за 16 дней! Вы потеряете 3-7 кг!', '962 руб/день', 'action', 1);
INSERT INTO blocks (header, price, block_name, enable) VALUES ('Полный день', '2500р', 'premium1', 1);
INSERT INTO blocks (header, price, block_name, enable) VALUES ('от 10-ти дней', '2200р/день', 'premium2', 1);

INSERT INTO blocks (header, price, block_name, enable) VALUES ('MorkovkaFit LIGHT', 'один день<br>1350р', 'ration1', 1);
INSERT INTO blocks (header, price, block_name, enable) VALUES ('MorkovkaFit LIGHT', 'от 10-ти дней<br>1250р/день', 'ration2', 1);
INSERT INTO blocks (header, price, block_name, enable) VALUES ('MorkovkaFit HARD', 'один день<br>1550р', 'ration3', 1);
INSERT INTO blocks (header, price, block_name, enable) VALUES ('MorkovkaFit HARD', 'от 10-ти дней<br>1450р/день', 'ration4', 1);
INSERT INTO blocks (header, price, block_name, enable) VALUES ('MorkovkaFit HARD+', 'один день<br>1650р', 'ration5', 1);
INSERT INTO blocks (header, price, block_name, enable) VALUES ('MorkovkaFit HARD+', 'от 10-ти дней<br>1550 р/день', 'ration6', 1);

INSERT INTO blocks (header, price, block_name, enable) VALUES ('один день', '1600р', 'unloading', 1);