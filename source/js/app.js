'use strict';

var scroller = require('./common/scroller');
var tabs = require('./common/tabs');
var map = require('./common/map');
var popups = require('./common/popups');
var scrollTo = require('./common/scroll-to');
var sendForm = require('./common/send-form');
var topButton = require('./common/top-button');
var policyInput = require('./common/policy-input');
var cookieMessage = require('./common/cookie-message');

$(function () {
  popups();
  tabs();
  scrollTo();
  map.init();
  scroller();
  sendForm();
  topButton();
  policyInput();
  cookieMessage();
});

