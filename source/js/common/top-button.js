module.exports = function init() {
    var $button = $('.js-top-button');
    var isActive = false;

    if (!$button.length) {
        return;
    }

    function showButton() {
        var scroll = $(window).scrollTop();

        if (scroll > 500 && !isActive) {
            $button.addClass('is-active');

            isActive = true;
        }

        if (scroll < 500 && isActive) {
            $button.removeClass('is-active');

            isActive = false;
        }
    }

    showButton();
    $(window).scroll(showButton);
};

