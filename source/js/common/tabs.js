'use strict';

function init() {
  var $container = $(this);
  var $navs = $container.find('.js-tabs__button');
  var $tabs = $container.find('.js-tabs__tab');

  function changeTab(targetId) {
    // Делаем все кнопки и вкладки неактивными
    $navs.filter('.active').removeClass('active');
    $tabs.filter('.active').removeClass('active');

    // Подсвечиваем активные
    $navs.filter('[href="' + targetId + '"]').addClass('active');
    $tabs.filter(targetId).addClass('active');
  }

  function changeTabOnClick(e) {
    e.preventDefault();

    changeTab($(this).attr('href'));
  }

  function changeTabOnHash() {
    var hash = window.location.hash;
    var $currentTab = $tabs.filter(hash);

    if ($currentTab.length) {
      changeTab(hash);
    }
  }

  changeTabOnHash();
  $navs.click(changeTabOnClick);
  $(window).on('hashchange', changeTabOnHash);
}

function tabs() {
  var $containers = $('.js-tabs');

  // Делаем группы табов атомарными
  $containers.each(init);
}

module.exports = tabs;