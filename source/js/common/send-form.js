'use strict';

function sendForm() {
  // Ввод по маске
  $('input[type="tel"]').mask("+7 999 999 99 99");
  $('input').placeholder();

  // Методы валидатора
  $.validator.addMethod('notPlaceholder', function (val, el) {
    return this.optional(el) || (val !== $(el).attr('placeholder'));
  }, $.validator.messages.required);

  $.validator.addMethod('phoneTest', function (val, el) {
    // console.log(val, /^\+7\s\d{3,3}\s\d{3,3}\s\d{2,2}\s\d{2,2}$/i.test(val));
    if (val == '+7 ___ ___ __ __' || val == '' || val == $(el).attr('placeholder')) {
      return true;
    }
    return /^\+7\s\d{3,3}\s\d{3,3}\s\d{2,2}\s\d{2,2}$/i.test(val);
  }, $.validator.messages.required);

  $.validator.addMethod('phoneRequired', function (val, el) {
    // console.log(val, !$(el).closest('form').hasClass('isTry2Submit'));
    return val == '+7 ___ ___ __ __' && $(el).closest('form').hasClass('isTry2Submit') ? false : true;
  }, $.validator.messages.required);

  // Валидация
  $('.js-form__ajax').each(function () {
    $(this).validate({
      rules: {
        name: {
          minlength: 3,
          required: true,
          notPlaceholder: true
        },
        phone: {
          phoneTest: true,
          phoneRequired: true,
          required: true,
          notPlaceholder: true
        }
      },
      messages: {
        name: {
          minlength: 'Минимум 3 символа',
          required: 'Вы не ввели имя',
          notPlaceholder: 'Вы не ввели имя'
        },
        phone: {
          phoneTest: 'Некорректный телефон',
          phoneRequired: 'Вы не ввели телефон',
          required: 'Вы не ввели телефон',
          notPlaceholder: 'Вы не ввели телефон'
        }
      },
      errorClass: 'error',
      validClass: 'valid',
      errorElement: 'span',
      onkeyup: false,
      errorPlacement: function (error, element) {
        element.attr("placeholder", error[0].outerText);
      },
      success: function () {
        alert('success');
      },
      showErrors: function (errorMap, errorList) {
        var submitButton = $(this.currentForm).find('button, input:submit');
        for (var error in errorList) {
          var element = $(errorList[error].element),
            errorMessage = errorList[error].message;
          element.val(errorMessage).addClass('error').attr('placeholder', errorMessage);
        }
      },
      submitHandler: function (form) {
        window.timerToCloseMsgPopup = null;
        $(form).toggleClass('__sending');
        // var goal = $(form).data('goal');
        $.ajax({
          type: 'POST',
          url: 'api/sendmail',
          data: $(form).serialize(),
          success: function (data) {
            $.magnificPopup.open({
              items: {
                src: $('#popup-thanks')
              },
              type: 'inline',
              midClick: true,
              showCloseBtn: false,
              removalDelay: 200,
              mainClass: 'mfp-fade',
              fixedContentPos: true
            }, 0);
            window.timerToCloseMsgPopup = setTimeout(function () {
              $.magnificPopup.close();
            }, 7000);
            $(form).toggleClass('__sending');
          },
          error: function (data) {
            // console.log(data);
            $.magnificPopup.close();
            $.magnificPopup.open({
              items: {
                src: $('#popup-error')
              },
              type: 'inline',
              midClick: true,
              showCloseBtn: false,
              removalDelay: 200,
              mainClass: 'mfp-fade',
              fixedContentPos: true
            }, 0);
            window.timerToCloseMsgPopup = setTimeout(function () {
              $.magnificPopup.close();
            }, 7000);
            $(form).toggleClass('__sending');
          }
        });
      }
    });
  });

  $('input').on('focus', function () {
    if ($(this).hasClass('error')) {
      $(this).val('').removeClass('error');
    }
  });

  $('button').click(function () {
    $(this).closest('form').addClass('isTry2Submit');
  });
}

module.exports = sendForm;