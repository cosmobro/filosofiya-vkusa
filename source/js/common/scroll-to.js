'use strict';

function scrollTo() {
  $(function () {
    // Высота меню в пикс
    var headerm = 0;
    var $buttons = $('.js-scroll-to');

    function scrollOnClick(e) {
      e.preventDefault();

      var $this = $(this);
      var $target = $($this.attr('href'));
      var hash = $this.data('rel') || '';

      function changeHash() {
        if (hash) {
          window.location.hash = hash;
        }
      }

      $('html, body').animate({
        scrollTop: $target.offset().top - headerm
      }, 800, changeHash);

      return false;
    }

    $buttons.click(scrollOnClick);
  });
}

module.exports = scrollTo;