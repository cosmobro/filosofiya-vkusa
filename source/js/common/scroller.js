'use strict';

function init() {
  var $animateGroups = $('.js-animate__group');

  $animateGroups.each(function () {
    var $group = $(this);
    var $elems = $group.find('.js-animate__elem');
    var $firstEl = $elems.eq(0);
    var firstStartOffset = $firstEl.offset().top;
    var lastEndOffset = $firstEl.offset().top + $firstEl.height();
    var tolerance = 200;

    function getFirstElement() {
      var $currentElement = $(this);
      var currentElementStartOffset = $currentElement.offset().top;

      if (currentElementStartOffset < firstStartOffset) {
        firstStartOffset = currentElementStartOffset;
      }
    }

    function getLastElement() {
      var $currentElement = $(this);
      var currentElementEndOffset = $currentElement.offset().top + $currentElement.height();

      if (currentElementEndOffset > lastEndOffset) {
        lastEndOffset = currentElementEndOffset;
      }
    }

    // Определяем первый к началу страницы и первый к концу страницы элемент из группы
    $elems.each(getFirstElement);
    $elems.each(getLastElement);

    $(window).on('scroll', function () {
      var scrollTopEdge = $(window).scrollTop();
      var scrollEndEdge = $(window).scrollTop() + $(window).height();

      if (scrollTopEdge > firstStartOffset + tolerance || !$elems.hasClass('animated')) {
        $elems.addClass('animated');
      }

      if ((scrollTopEdge > lastEndOffset || scrollEndEdge < firstStartOffset + tolerance) && $elems.hasClass('animated')) {
        $elems.removeClass('animated');
      }
    });
  });
}

module.exports = init;