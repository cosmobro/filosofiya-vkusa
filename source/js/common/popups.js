'use strict';
function init() {
  var $popupOpeners = $('.js-popup__opener');
  var $closeButton = $('.js-popup__close');
  var magnificPopupOptions = {
    type: 'inline',
    midClick: true,
    showCloseBtn: false,
    removalDelay: 200,
    mainClass: 'mfp-fade',
    fixedContentPos: true
  };

  $popupOpeners.magnificPopup(magnificPopupOptions);

  $closeButton.click($.magnificPopup.close);
}

module.exports = init;