function init() {
  var $container = $(this);
  var $checkbox = $container.find('.js-policy-checkbox');
  var $button = $container.find('.js-policy-button');

  function changeButtonState(isDisabled) {
    $button.attr('disabled', isDisabled);
  }

  changeButtonState($checkbox.is(':not(:checked)'));

  $checkbox.change(function () {
    changeButtonState($checkbox.is(':not(:checked)'));
  });
}

module.exports = function () {
  const $policy = $('.js-policy');

  $policy.each(init);
};
