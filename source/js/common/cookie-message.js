module.exports = function () {
  var $cookieMessages = $('.js-cookie-message');

  $cookieMessages.each(function () {
    var $container = $(this);
    var $closeButton = $container.find('.js-cookie-message-close');

    console.log(getCookie('cookieAgree'));

    if (!getCookie('cookieAgree'))
      $container.addClass('is-enabled');

    if (!$closeButton.length)
      return;

    $closeButton.click(function () {
      document.cookie = 'cookieAgree=true;path=/';

      $container.removeClass('is-enabled');
    });
  });

  function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }
}