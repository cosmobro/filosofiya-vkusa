'use strict';

module.exports = [
  './node_modules/selectivizr2/selectivizr2.js',
  './node_modules/jquery/dist/jquery.js',
  './node_modules/magnific-popup/dist/jquery.magnific-popup.js',
  './node_modules/jquery-validation/dist/jquery.validate.js',
  './node_modules/jquery-placeholder/jquery.placeholder.js',
  './node_modules/jquery.maskedinput/src/jquery.maskedinput.js'
];
