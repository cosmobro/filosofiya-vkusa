'use strict';

module.exports = function () {
  $.gulp.task('pug', function () {
    const fs = require('fs');

    return $.gulp.src('./source/template/pages/*.pug')
      .pipe($.gp.pug({
        locals: JSON.parse(fs.readFileSync($.config.dataPath, 'utf-8')),
        pretty: true
      }))
      .on('error', $.gp.notify.onError(function (error) {
        return {
          title: 'Pug',
          message: error.message
        }
      }))
      // .pipe($.gp.rename({extname: '.php'}))
      .pipe($.gulp.dest($.config.root + '/ui'));
  });
};
