'use strict';

module.exports = function () {
  $.gulp.task('inline:styles', function () {
    return $.gulp.src([$.config.root + '/ui/*.html', $.config.root + '/ui/*.php'])
      .pipe($.gp.replace('href="assets/', 'href="../assets/'))
      .pipe($.gp.inlineSource())
      .pipe($.gp.replace('url(\'../', 'url(\'assets/'))
      .pipe($.gp.replace('url(../', 'url(assets/'))
      .pipe($.gulp.dest($.config.root + '/ui'));
  })
};